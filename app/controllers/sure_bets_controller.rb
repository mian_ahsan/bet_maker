class SureBetsController < ApplicationController
  # GET /sure_bets
  # GET /sure_bets.json
  def index
    @sure_bets = SureBet.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sure_bets }
    end
  end

  # GET /sure_bets/1
  # GET /sure_bets/1.json
  def show
    @sure_bet = SureBet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sure_bet }
    end
  end

  # GET /sure_bets/new
  # GET /sure_bets/new.json
  def new
    @sure_bet = SureBet.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sure_bet }
    end
  end

  # GET /sure_bets/1/edit
  def edit
    @sure_bet = SureBet.find(params[:id])
  end

  # POST /sure_bets
  # POST /sure_bets.json
  def create
    @sure_bet = SureBet.new(params[:sure_bet])

    respond_to do |format|
      if @sure_bet.save
        format.html { redirect_to @sure_bet, notice: 'Sure bet was successfully created.' }
        format.json { render json: @sure_bet, status: :created, location: @sure_bet }
      else
        format.html { render action: "new" }
        format.json { render json: @sure_bet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sure_bets/1
  # PUT /sure_bets/1.json
  def update
    @sure_bet = SureBet.find(params[:id])

    respond_to do |format|
      if @sure_bet.update_attributes(params[:sure_bet])
        format.html { redirect_to @sure_bet, notice: 'Sure bet was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sure_bet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sure_bets/1
  # DELETE /sure_bets/1.json
  def destroy
    @sure_bet = SureBet.find(params[:id])
    @sure_bet.destroy

    respond_to do |format|
      format.html { redirect_to sure_bets_url }
      format.json { head :no_content }
    end
  end
end
