require 'test_helper'

class SureBetsControllerTest < ActionController::TestCase
  setup do
    @sure_bet = sure_bets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sure_bets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sure_bet" do
    assert_difference('SureBet.count') do
      post :create, sure_bet: {  }
    end

    assert_redirected_to sure_bet_path(assigns(:sure_bet))
  end

  test "should show sure_bet" do
    get :show, id: @sure_bet
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sure_bet
    assert_response :success
  end

  test "should update sure_bet" do
    put :update, id: @sure_bet, sure_bet: {  }
    assert_redirected_to sure_bet_path(assigns(:sure_bet))
  end

  test "should destroy sure_bet" do
    assert_difference('SureBet.count', -1) do
      delete :destroy, id: @sure_bet
    end

    assert_redirected_to sure_bets_path
  end
end
